//import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `java-library`
    kotlin("jvm")
    kotlin("kapt")
    kotlin("plugin.serialization")
}

dependencies {
    //implementation fileTree(dir: 'libs', include: ['*.jar'])
    //implementation "org.jetbrains.kotlin:kotlin-stdlib-jdk7:$kotlin_version"
    // implementation project(path: ':libbitcoincash')

    implementation(kotlin("reflect"))
    implementation(kotlin("stdlib-jdk8"))

    implementation("org.jetbrains.kotlinx", "kotlinx-coroutines-core", "1.3.9")
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-cbor", "1.0.1")
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-core", "1.0.1")
    implementation("org.jetbrains.kotlinx", "kotlinx-serialization-json", "1.0.1")

    implementation("org.jetbrains.exposed", "exposed-core", "0.25.1")
    implementation("org.jetbrains.exposed", "exposed-dao", "0.25.1")
    implementation("org.jetbrains.exposed", "exposed-jdbc", "0.25.1")

    // gradle database dependencies described here: https://github.com/JetBrains/Exposed/wiki/DataBase-and-DataSource
    implementation("org.xerial","sqlite-jdbc","3.21.0.1")
    implementation("com.h2database","h2","1.4.197")

}

/*
sourceSets {
    main {
        java.srcDir("src/core/java")
    }
}
 */

java {
    sourceCompatibility = JavaVersion.VERSION_11
    targetCompatibility = JavaVersion.VERSION_11
}

tasks {
    test {
        testLogging.showExceptions = true
    }
}