# libbitcoincashkotlin

Kotlin library that wraps libbitcoincash.so and provides additional blockchain functionality.


## Dependencies

### libbitcoincash

#### Debian/Ubuntu

```bash
apt-get install cmake python3 libtool automake build-essential ninja-build
```

Install cmake 3.12 or greater.  This installs a modern cmake from kitware.

```bash
wget -O - https://apt.kitware.com/keys/kitware-archive-latest.asc 2>/dev/null | gpg --dearmor - | sudo tee /etc/apt/trusted.gpg.d/kitware.gpg >/dev/null
sudo apt-add-repository 'deb https://apt.kitware.com/ubuntu/ bionic main'
sudo apt-get update
sudo apt-get install cmake
cmake --version
```

```
cmake version 3.18.4
CMake suite maintained and supported by Kitware (kitware.com/cmake).
```

#### Mac
```bash
brew install cmake ninja
```

### Android SDK Installation

- Go to https://developer.android.com/studio/index.html
- Find section "Command Line Tools only"
- Download `commandlinetools-<platform>-XXXXXXX_latest.zip`
- Unzip and set envoronment var `ANDROID_SDK_ROOT` to the unzipped folder

### Android SDK Licenses

You'll need to accept a bunch of licenses in order to proceed. Run
`./accept-licenses.sh`.

### Android NDK Installation

- Download version r21b from https://developer.android.com/ndk/downloads
    - For Linux, the direct URL is: https://dl.google.com/android/repository/android-ndk-r21b-linux-x86_64.zip
- Unzip and set environment var `ANDROID_NDK_ROOT` to the unzipped folder

## Troubleshooting

### Cannot accept NDK license

sdkmanager does not support Java 11 and you either need to downgrade to older
version of Java, or hack it to use older version.
See: https://github.com/flutter/flutter/issues/16025#issuecomment-468009198

### UnsatisfiedLinkError

Error similar to
```
    java.lang.UnsatisfiedLinkError: No implementation found for byte[] bitcoinunlimited.libbitcoincash.Hash.sha256(byte[]) (tried Java_bitcoinunlimited_libbitcoincash_Hash_sha256 and Java_bitcoinunlimited_libbitcoincash_Hash_sha256___3B)
```

Solution: Load and initialize the library:
```
System.loadLibrary("bitcoincashandroid")
Initialize.LibBitcoinCash(ChainSelector.BCHMAINNET.v)
```
