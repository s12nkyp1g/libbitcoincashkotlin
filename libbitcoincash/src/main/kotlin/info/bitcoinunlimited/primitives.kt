// Copyright (c) 2019 Andrew Stone Consulting (qq9wwnuw4eukyh5g34ckg5vk4aaxnvr04vkspyv850)
// Distributed under the MIT software license, see the accompanying file COPYING or http://www.opensource.org/licenses/mit-license.php.
package bitcoinunlimited.libbitcoincash


private val LogIt = GetLog("BU.primitives")

//import jetbrains.exodus.crypto.toBinaryKey

open class PayAddressException(msg: String, shortMsg: String? = null, severity: ErrorSeverity = ErrorSeverity.Expected): BUException(msg, shortMsg, severity)
open class PayAddressDecodeException(msg: String): PayAddressException(msg, appI18n(RbadAddress))
open class PayAddressBlankException(msg: String): PayAddressException(msg, appI18n(RblankAddress), ErrorSeverity.Expected)


fun DbgRender(obj: Hash256):String
{
    return obj.toHex()
}

data class Hash256(val hash:ByteArray = ByteArray(32, { _ -> 0 })): BCHserializable()
{
    init {
        assert(hash.size == 32)
    }

    constructor(hex: String):this()
    {
        val hsh = hex.FromHex()
        hsh.reverse()
        assert(hsh.size == 32)
        hsh.copyInto(hash)
    }

    constructor(stream: BCHserialized):this()
    {
        BCHdeserialize(stream)
    }

    operator fun get(i:Int) = hash[i]
    operator fun set(i: Int, b:Byte)
    {
        hash[i] = b
    }

    /** Convert to the bitcoin standard hex representation */
    fun toHex(): String
    {
        val cpy = hash.copyOf()
        cpy.reverse()
        return ToHexStr(cpy)
    }

    /** The default display will be bitcoin standard hex representation (reversed hex) */
    override fun toString(): String = toHex()

    override fun equals(other: Any?): Boolean
    {
        if (other is Hash256) return hash contentEquals other.hash
        return false
    }

    operator fun compareTo(h: Any?): Int
    {
        if (h is Hash256)
        {
            for (i in 0..32)
            {
                if (hash[i] < h.hash[i]) return -1
                else if (hash[i] > h.hash[i]) return 1
            }
            return 0
        }
        return -1
    }

    override fun hashCode(): Int
    {
        return hash[1].toUint() shl 24 or hash[7].toUint() shl 16 or hash[17].toUint() shl 8 or hash[30].toUint()
    }

    override fun BCHserialize(format: SerializationType): BCHserialized = BCHserialized(hash, format)

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        stream.debytes(32).copyInto(hash)
        return stream
    }
}

fun BCHserialized.denullHash(): Hash256?
{
    val v = Hash256()
    v.BCHdeserialize(this)
    if (v == Hash256()) return null
    return v
}


// Globally unique identifier, implemented as the hash256 of the object
class Guid(var data: Hash256 = Hash256()): BCHserializable()
{
    val GUID_LEN_BYTES:Int = 32

    constructor(dat: String):this(Hash256(dat.FromHex()))
    {
        assert(dat.length == GUID_LEN_BYTES*2)
    }
    constructor(dat: ByteArray):this()
    {
        assert(dat.size == GUID_LEN_BYTES)
        data = Hash256(dat)
    }

    fun toHex(): String = data.toHex()

    // Serialization
    constructor(dat: BCHserialized): this()
    {
        data = Hash256(dat.debytes(GUID_LEN_BYTES.toLong()))
    }
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        var ret = BCHserialized(format) + exactBytes(data.hash)  //.reversedArray()
        return ret
    }
    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        data = Hash256(stream.debytes(GUID_LEN_BYTES.toLong()))
        return stream
    }

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is Guid)
        {
            return data.equals(other.data)
        }
        return false
    }

}



/**
 An array of block hashes, starting at the current block, and ending at the genesis block.  The creator can skip
 blocks, populating whatever hashes he feels is most likely to identify a specific chain, closest to the splitoff point.
 Typically, some kind of exponential backoff is used.  For example:
 The next 10 hashes are the previous 10 blocks (gap 1).
 After these, the gap is multiplied by 2 for each hash, until the genesis block is added
 */
class BlockLocator: BCHserializable()
{
    var have: MutableList<Hash256> = mutableListOf()

    fun add(b: Hash256): BlockLocator
    {
        have.add(b)
        return this
    }
    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        //return BCHserialized.int32(BCHserialized.SER_NETWORK, format) + have
        return BCHserialized.int32(0, format) + have
    }

    override fun toString():String
    {
        var ret: String = "["
        for (h in have)
        {
            ret += h.toHex() + " "
        }
        ret += "]"
        return ret
    }

}


// Payment Address abstraction
enum class  PayAddressType(val v:Byte)
{
    NONE(0), P2PUBKEY(1), P2PKH(2), P2SH(3)  // Must match C++ cashlib.cpp PubkeyExtractor
}

fun LoadPayAddressType(v:Byte): PayAddressType
{
    return when(v)
    {
        0.toByte() -> PayAddressType.NONE
        1.toByte() -> PayAddressType.P2PUBKEY
        2.toByte() -> PayAddressType.P2PKH
        3.toByte() -> PayAddressType.P2SH
        else -> throw PayAddressDecodeException("unknown address type")
    }
}

data class PayAddress(var blockchain: ChainSelector, var type: PayAddressType, var data: ByteArray):BCHserializable()
{
    constructor(stream: BCHserialized): this(ChainSelector.BCHMAINNET, PayAddressType.P2PUBKEY, ByteArray(0))
    {
        BCHdeserialize(stream)
    }

    constructor(address: String):this(ChainSelectorFromAddress(address), PayAddressType.P2PUBKEY,ByteArray(0))
    {

        if (address == "") throw PayAddressBlankException("")
        val decoded = DecodeCashAddr(blockchain.v, address)
        type = LoadPayAddressType(decoded[0])
        data = decoded.sliceArray(IntRange(1, 20))
    }

    override fun BCHserialize(format: SerializationType): BCHserialized
    {
        if (format == SerializationType.DISK)
        {
            return BCHserialized(format) + BCHserialized.uint8(blockchain.v) + BCHserialized.uint8(type.v) + variableSized(data)
        }
        else if (format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
            // return BCHserialized(format) + exactBytes(data)
        }
        throw NotImplementedError()
    }

    override fun BCHdeserialize(stream: BCHserialized): BCHserialized
    {
        if (stream.format == SerializationType.DISK)
        {
            blockchain = ChainSelectorFromValue(stream.deuint8())
            type = LoadPayAddressType(stream.deuint8())
            data = stream.deByteArray()
        }
        else if (stream.format == SerializationType.NETWORK)  // in scripts for example, we just place the raw bytes
        {
            throw NotImplementedError()
            // data = stream.debytes(20)
        }
        return stream
    }


    override fun toString(): String
    {
        if (type == PayAddressType.NONE) {
            throw java.lang.IllegalArgumentException("Invalid address type")
        }
        if (data.size != 20) {
            throw java.lang.IllegalArgumentException("Invalid dataload. Expected 20 bytes, got " + data.size)
        }
        return EncodeCashAddr(blockchain.v, type.v, data)
    }

    /** return the prefix for this address, e.g. "bchreg" in "bchreg:qpvdragqrmvashle90kjjx7hx87aq6xe75jlnlxc9c" */
    val addressUriScheme: String
        get() = chainToURI[blockchain]!!  // !! because its a coding error if this dictionary doesn't contain every ChainSelector enum item

    /** @return the script that constrains spending to this address */
    fun outputScript():BCHscript
    {
        val script = when (type)
        {
            PayAddressType.NONE     -> throw WalletNotSupportedException("Cannot create payment unconstrained by an address")
            PayAddressType.P2PUBKEY -> throw WalletNotSupportedException("Pay to public key outputs not supported")
            PayAddressType.P2PKH    -> BCHscript(blockchain, OP.DUP, OP.HASH160, OP.push(data), OP.EQUALVERIFY, OP.CHECKSIG)
            PayAddressType.P2SH     -> BCHscript(blockchain, OP.HASH160, OP.push(data), OP.EQUAL)
            // unnecessary: else                    -> throw PayAddressException("Payment address not supported")
        }
        return script
    }

    /** synonym for outputScript()
     * @return the script that constrains spending to this address
     */
    fun constraintScript(): BCHscript = outputScript()

    override fun equals(other: Any?): Boolean
    {
        return contentEquals(other)
        //return super.equals(other)
    }

    fun contentEquals(other: Any?): Boolean
    {
        if (other == null) return false
        if (other is PayAddress)
        {
            return ((type == other.type)&&(data.contentEquals(other.data)))
        }
        return false
    }

    override fun hashCode(): Int
    {
        val dhc = data.contentHashCode()
        val hc = type.ordinal.toInt().xor(dhc)
        return hc
    }

    companion object {
        @JvmStatic
        external fun EncodeCashAddr(chainSelector: Byte, type: Byte, data: ByteArray): String
        @JvmStatic
        external fun DecodeCashAddr(chainSelector: Byte, addr: String): ByteArray
    }
}
