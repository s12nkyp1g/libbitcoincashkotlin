package bitcoinunlimited.libbitcoincash

open class UnknownBlockchainException() : BUException(
        "Could not determine the a blockchain from the input",
        "Unknown cryptocurrency/blockchain")

/***
 * Identify the cryptocurrency/blockchain.
 */
enum class ChainSelector(val v:Byte)
{
    BCHMAINNET(1), BCHTESTNET(2), BCHREGTEST(3), BCHNOLNET(4), NEXTCHAIN(5)
}

fun ChainSelectorFromValue(v: Byte): ChainSelector
{
    return when(v)
    {
        ChainSelector.BCHMAINNET.v -> ChainSelector.BCHMAINNET
        ChainSelector.BCHTESTNET.v -> ChainSelector.BCHTESTNET
        ChainSelector.BCHREGTEST.v -> ChainSelector.BCHREGTEST
        ChainSelector.BCHNOLNET.v -> ChainSelector.BCHNOLNET
        ChainSelector.NEXTCHAIN.v -> ChainSelector.NEXTCHAIN
        else -> throw UnknownBlockchainException()
    }
}

/** Convert a ChainSelector to its uri and address prefix */
val chainToURI: Map<ChainSelector, String> = mapOf(ChainSelector.BCHMAINNET to "bitcoincash", ChainSelector.BCHTESTNET to "bchtest", ChainSelector.BCHREGTEST to "bchreg",
                                                   ChainSelector.BCHNOLNET to "bchnol", ChainSelector.NEXTCHAIN to "xnex")
/** Convert a uri or address prefix to a ChainSelector -- throws exception if not found */
val uriToChain: Map<String, ChainSelector> = chainToURI.invert()

/** Convert a ChainSelector to its uri and address prefix */
val chainToCurrencyCode: Map<ChainSelector, String> = mapOf(ChainSelector.BCHMAINNET to "BCH", ChainSelector.BCHTESTNET to "TBCH", ChainSelector.BCHREGTEST to "RBCH",
    ChainSelector.BCHNOLNET to "NBCH",ChainSelector.NEXTCHAIN to "XNEX")

/** Convert a ChainSelector to its milli-currency code*/
val chainToMilliCurrencyCode: Map<ChainSelector, String> = mapOf(ChainSelector.BCHMAINNET to "mBCH", ChainSelector.BCHTESTNET to "mTBCH", ChainSelector.BCHREGTEST to "mRBCH",
    ChainSelector.BCHNOLNET to "mNBCH", ChainSelector.NEXTCHAIN to "mXNEX")


/** Convert a uri or address prefix to a ChainSelector -- throws exception if not found */
val currencyCodeToChain: Map<String, ChainSelector> = chainToURI.invert()


fun ChainSelectorFromAddress(address:String):ChainSelector
{
    try
    {
        if (address.contains(":"))
        {
            val (chain, _) = address.split(":")
            return uriToChain[chain] ?: throw UnknownBlockchainException()
        }
        else
        {
            if ((address[0] == '1') || (address[0] == '3')) throw UnknownBlockchainException()  // TODO bitcoin address
            if ((address[0] == 'q') || (address[0] == 'p')) return ChainSelector.BCHMAINNET
        }
    }
    catch(e: IndexOutOfBoundsException)
    {
        // no colon means missing the blockchain identifier prefix used in bitcoincash
        // TODO handle the case where people forget to use the chain identifier
        // LogIt.info("Bad address, cannot determine blockchain: '$address'")
        throw UnknownBlockchainException()
    }
    throw UnknownBlockchainException()
}

