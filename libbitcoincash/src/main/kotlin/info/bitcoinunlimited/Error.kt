package bitcoinunlimited.libbitcoincash

// It is necessary to abstract the R.string. resource identifier because they don't exist on other platforms
val RinsufficentBalance = 0xf00d + 0 // R.string.insufficentBalance
val RbadWalletImplementation = 0xf00d + 1 // R.string.badWalletImplementation
val RdataMissing = 0xf00d + 2 // R.string.dataMissing
val RwalletAndAddressIncompatible = 0xf00d + 3 // R.string.chainIncompatibleWithAddress
val RnotSupported = 0xf00d + 4 // R.string.notSupported
val Rexpired = 0xf00d + 5 // R.string.expired
val RsendMoreThanBalance = 0xf00d + 6 // R.string.sendMoreThanBalance
val RbadAddress = 0xf00d + 7 // R.string.badAddress
val RblankAddress = 0xf00d + 8 // R.string.blankAddress
val RblockNotForthcoming = 0xf00d + 9 // R.string.blockNotForthcoming
val RheadersNotForthcoming = 0xf00d + 10 // R.string.headersNotForthcoming
val RbadTransaction = 0xf00d + 11 // R.string.badTransaction
val RfeeExceedsFlatMax = 0xf00d + 12 // R.string.feeExceedsFlatMax
val RexcessiveFee =  0xf00d + 13 // R.string.excessiveFee
val Rbip70NoAmount = 0xf00d + 14 //  R.string.badAmount
val RdeductedFeeLargerThanSendAmount =  0xf00d + 15 // R.string.deductedFeeLargerThanSendAmount
var RwalletDisconnectedFromBlockchain =  0xf00d + 16 // R.string.walletDisconnectedFromBlockchain
val RsendDust = 0xf00d + 17 // R.string.sendDustError
val RnoNodes = 0xf00d + 18 // R.string.noNodes


//* Indicates how severe the exceptions is
enum class ErrorSeverity
{
    Expected,  //* This is a normal error that should be handled programatically.  No logging necessary
    Abnormal,  //* This is unexpected but worst case means that minor functionality is unavailable.  Log it
    Severe,    //* Causes major functionality to be unavailable. Log it
}

/**
 * @property msg Full description of the problem.
 * @property shortMsg The exception "title". Useful for displaying in a the title bar
 * @property severity Indicates how the error should be logged and handled.
 */
open class BUException(msg: String, val shortMsg: String? = null, val severity: ErrorSeverity = ErrorSeverity.Abnormal): Exception(msg)

open class UnimplementedException(msg: String, shortMsg: String? = null): BUException(msg, shortMsg, ErrorSeverity.Abnormal)
