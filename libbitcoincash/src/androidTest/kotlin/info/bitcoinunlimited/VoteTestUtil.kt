package info.bitcoinunlimited

import bitcoinunlimited.libbitcoincash.TwoOptionVote
import bitcoinunlimited.libbitcoincash.TwoOptionVoteContract
import bitcoinunlimited.libbitcoincash.UtilStringEncoding

class VoteTestUtil {
    companion object {
        @JvmStatic
        fun contractInstances(): List<TwoOptionVoteContract> {
            val salt = "unittest".toByteArray()
            val description = "Foo?"
            val optA = "Bar"
            val optB = "Baz"
            val optAHash = TwoOptionVote.hash160_salted(salt, optA.toByteArray())
            val optBHash = TwoOptionVote.hash160_salted(salt, optB.toByteArray())
            val endHeight = 642042
            val participants = arrayOf(
                UtilStringEncoding.hexToByteArray("0e2f5d8a017c4983cb56320d18f66bf01587aa44"),
                UtilStringEncoding.hexToByteArray("3790bb07029831ec90f8eb1ed7c1c09aac178185"),
                UtilStringEncoding.hexToByteArray("aaa8a14f0658c44809580b24299a592d7623976c"),
                UtilStringEncoding.hexToByteArray("9b1772d9287b9a3587b0a1e147f34714697c780c"),
                UtilStringEncoding.hexToByteArray("ca9c60699d3d4c6b71b1fa5b71934feaf9f5a004"),
                UtilStringEncoding.hexToByteArray("f7d1358c4baa20b31e5f84c238e964473d733f75")
            )

            val id = TwoOptionVote.calculate_proposal_id(
                salt, description, optA, optB, endHeight, participants
            )

            return participants.map { TwoOptionVoteContract(id, optAHash, optBHash, it) }
        }

        @JvmStatic
        fun mocPrivateKey(fill: Char = 'A'): ByteArray = ByteArray(32) { _ -> fill.toByte() }
    }
}
